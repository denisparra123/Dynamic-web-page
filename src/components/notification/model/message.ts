export interface MessageNotifier {
  messageIsInactive(message: Message);
}

export class Message {
  id: string;
  isUnResolved: boolean;
  isActive: boolean;
  type: string;
  content: string;
  owner: string;
  icon: string;
  listeners: MessageNotifier[];

  constructor(content, type, owner, icon) {
    this.isActive = true;
    this.isUnResolved = true;
    this.content = content;
    this.type = type;
    this.owner = owner;
    this.icon = icon;
    this.id = Math.random().toString(36).substr(2, 9);
  }

  start() {
    setTimeout(() => {
      this.processMessage();
    }, 5000);
  }

  setInactiveMessage() {
    setTimeout(() => {
      this.isActive = false;
      this.notify();
    }, 1000);
  }

  end() {
    this.processMessage();
  }

  private processMessage() {
    if (this.isUnResolved) {
      this.isUnResolved = false;
      this.setInactiveMessage();
    }
  }

  private notify() {
    if (this.listeners) {
      this.listeners.forEach(listener => {
        listener.messageIsInactive(this);
      });
    }
  }
}
