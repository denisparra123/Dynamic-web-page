import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material';

import { NotificationComponent } from './notification.component';
import { NotificationService } from './notification.service';

@NgModule({
  declarations: [ NotificationComponent],
  imports: [
      CommonModule,
      TranslateModule,
      MatIconModule,
  ],
  exports: [NotificationComponent]
})
export class NotificationModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: NotificationModule,
      providers: [ NotificationService ]
    };
  }
}
