import { Message } from '../model/message';

export function buildMessage(content: string, type: string, owner: string, icon: string): Message {
  return new Message(content, type, owner, icon);
}
