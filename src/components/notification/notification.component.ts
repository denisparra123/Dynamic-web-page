import { Component, OnInit, Input } from '@angular/core';

import { NotificationService } from './notification.service';
import { Message, MessageNotifier } from './model/message';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.less'],
})
export class NotificationComponent implements MessageNotifier, OnInit {
  @Input() owner;
  messages: Message[] = [];

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    this.notificationService.getMessage().subscribe(
      message => {
        if (message.owner === this.owner) {
            this.messages.push(message);
            message.start();
        }
      }
    );
  }

  messageIsInactive(messageInactived: Message) {
    const index = this.messages.findIndex(message => message.id === messageInactived.id);
    if (index > -1) {
      this.messages.splice(index, 1);
    }
  }

  closeMessage(message: Message) {
    message.end();
  }
}
