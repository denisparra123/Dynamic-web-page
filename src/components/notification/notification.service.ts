import { Injectable } from '@angular/core';

import { Observable, ReplaySubject } from 'rxjs';

import { Message } from './model/message';

@Injectable()
export class NotificationService {
  subject: ReplaySubject<Message> = new ReplaySubject();

  addMessage(message: Message) {
    this.subject.next(message);
  }

  getMessage(): Observable<Message> {
    return this.subject.asObservable();
  }
}
