import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry } from '@angular/material';

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.less']
})

export class FooterComponent {
    constructor(private matIconRegistry: MatIconRegistry,
                private sanitizer: DomSanitizer) {
                    matIconRegistry.addSvgIcon('facebook-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/facebook.svg'));
                    matIconRegistry.addSvgIcon('twitter-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/twitter.svg'));
                    matIconRegistry.addSvgIcon('youtube-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/youtube.svg'));
                    matIconRegistry.addSvgIcon('instagram-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/instagram.svg'));
                    matIconRegistry.addSvgIcon('whatsapp-icon', sanitizer.bypassSecurityTrustResourceUrl('assets/icons/whatsapp.svg'));
                }
}
