import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { NotificationModule } from '../../components/notification/notification.module';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home.routing.module';

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    HomeRoutingModule,
    TranslateModule,
    NotificationModule
  ]
})
export class HomeModule { }
