import { Component, OnInit } from '@angular/core';

import { NotificationService } from '../../components/notification/notification.service';
import * as MessageBuilder from '../../components/notification/builder/message.builder';

@Component({
  selector: 'app-home',
  styleUrls: ['./home.component.less'],
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {
  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    setTimeout(() => {
      this.notificationService.addMessage(
        MessageBuilder.buildMessage('MESSAGE.INFO.TEST', 'info', 'home-notification', 'error')
      );
    }, 5000);
  }
}
