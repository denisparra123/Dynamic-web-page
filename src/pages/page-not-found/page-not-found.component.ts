import { Component } from '@angular/core';

@Component({
    selector: 'app-not-found-page',
    styleUrls: ['./page-not-found.component.css'],
    templateUrl: './page-not-found.component.html'
})
export class PageNotFoundComponent {}
