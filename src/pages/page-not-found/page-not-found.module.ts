import { NgModule } from '@angular/core';

import { TranslateModule } from '@ngx-translate/core';

import { PageNotFoundComponent } from './page-not-found.component';
import { PageNotFoundRoutingModule } from './page-not-found.routing.module';

@NgModule({
  declarations: [
    PageNotFoundComponent
  ],
  imports: [
    PageNotFoundRoutingModule,
    TranslateModule
  ]
})
export class PageNotFoundModule { }
