export interface NavBarMenuItem {
  route: string;
  color: string;
  haveIcon: boolean;
  text: string;
  icon?: string;
}
