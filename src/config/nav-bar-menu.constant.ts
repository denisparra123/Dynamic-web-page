import { NavBarMenuItem } from '../models/nav-bar-menu.model';

export const NAV_BAR_MENU_ITEMS: NavBarMenuItem[] = [
  {
    color: 'basic',
    haveIcon: false,
    route: 'page-not-found',
    text: 'SIDE_NAV.MISSION'
  },
  {
    color: 'basic',
    haveIcon: false,
    route: 'page-not-found',
    text: 'SIDE_NAV.VISION'
  },
  {
    color: 'basic',
    haveIcon: false,
    route: 'page-not-found',
    text: 'SIDE_NAV.ABOUT_US'
  },
  {
    color: 'basic',
    haveIcon: false,
    route: 'page-not-found',
    text: 'SIDE_NAV.HOW_TO_GET'
  },
  {
    color: 'warn',
    haveIcon: true,
    route: 'page-not-found',
    text: 'SIDE_NAV.MAKE_AN_APPOINTMENT',
    icon: 'event'
  }
];
